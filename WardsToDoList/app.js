var itemTemplates = $('#templates .item')
var list = $('#list')

var addItemToPage = function(itemData){
    var item = itemTemplates.clone()
    item.attr('data-id', itemData.id)
    item.find('.description').text(itemData.description)
    if(itemData.completed){
        item.addClass('completed')
    }
    list.append(item)
}

var loadRequest = $.ajax({
    method:'GET',
    url:'https://listalous.herokuapp.com/lists/Wards-List'
  });

  loadRequest.done(function(dataFromServer) {
  var itemsData = dataFromServer.items

  itemsData.forEach(function(itemData) {
    addItemToPage(itemData)
  })
})


$('#add-form').on('submit', function(event) {
  event.preventDefault()
  var itemDescription = event.target.itemDescription.value
  var creationRequest = $.ajax({
     type: 'POST',
     url: "https://listalous.herokuapp.com/lists/Wards-List/items",
     data: { description: itemDescription, completed: false }
   })

  creationRequest.done(function(itemDataFromServer) {
    addItemToPage(itemDataFromServer)
  })
  $('#create').val('').blur()
})

$('#list').on('click', '.complete-button', function(event) {
  var item = $(event.target).parent()
var isItemCompleted = item.hasClass('completed')
var itemId = item.attr('data-id')
var updateRequest = $.ajax({
  type: 'PUT',
  url: "https://listalous.herokuapp.com/lists/Wards-List/items/" + itemId,
  data: { completed: !isItemCompleted }
})

updateRequest.done(function(itemData) {
  if (itemData.completed) {
    item.addClass('completed')
  } else {
    item.removeClass('completed')
  }
})
})

$('#list').on('click', '.delete-button', function(event) {
  var item = $(event.target).parent()

var itemId = item.attr('data-id')
var removeRequest = $.ajax({
  type: 'DELETE',
  url: "https://listalous.herokuapp.com/lists/Wards-List/items/" + itemId
})
item.remove()
})